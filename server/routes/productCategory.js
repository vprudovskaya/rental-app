const express = require('express');
const router = express.Router();
const controller = require('../controllers/productCategory');

// localhost:5000/api/productCategory/
router.post('/', controller.post);

// localhost:5000/api/productCategory/:categoryName
router.get('/:categoryName', controller.getProductsByCategoryName);

module.exports = router;
