const express = require('express');
const router = express.Router();
const controller = require('../controllers/organization-info');

// localhost:5000/api/organization-info/
router.post('/', controller.set);

// localhost:5000/api/organization-info/
router.get('/', controller.get);

module.exports = router;
