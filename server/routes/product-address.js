const express = require('express');
const router = express.Router();
const controller = require('../controllers/product-address');

// localhost:5000/api/productAddress/
router.post('/', controller.add);

// localhost:5000/api/productAddress/:id
router.get('/:id', controller.getAllByProductId)


module.exports = router;
