const express = require('express');
const router = express.Router();
const controller = require('../controllers/product');

// http://localhost:5000/api/product/
router.post('/', controller.create);

//http://localhost:5000/api/product/:id
router.get('/:id', controller.getById);

//http://localhost:5000/api/product/
router.get('/', controller.getAll);

//
// router.get('/products/:category', );

module.exports = router;
