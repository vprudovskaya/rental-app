const express = require('express');
const router = express.Router();
const controller = require('../controllers/pick-up-point');

// localhost:5000/api/pick-up-point/
router.post('/', controller.create);

router.get('/', controller.getAll);

module.exports = router;
