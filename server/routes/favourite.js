const express = require('express');
const router = express.Router();
const controller = require('../controllers/favourite');

// localhost:5000/api/favourite/
router.post('/', controller.setFavourite);

// localhost:5000/api/favourite/
router.delete('/', controller.deleteFavourite);

// localhost:5000/api/favourite/
router.get('/', controller.checkFavourite);

//
router.get('/:id', controller.getAllById);


module.exports = router;
