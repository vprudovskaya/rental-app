const express = require('express');
const passport = require('passport');
const router = express.Router();
const controller = require('../controllers/order');

// http://localhost:5000/api/order/
router.post('/', controller.create);

// http://localhost:5000/api/order/
router.get('/', controller.getAllById);

module.exports = router;
