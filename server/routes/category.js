const express = require('express');
const router = express.Router();
const controller = require('../controllers/category');

// localhost:5000/api/category/
router.post('/', controller.create);

// localhost:5000/api/category/
router.get('/', controller.getAll);

module.exports = router;
