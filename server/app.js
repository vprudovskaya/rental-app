const authRoutes = require('./routes/auth');
const orderRoutes = require('./routes/order');
const productRoutes = require('./routes/product');
const favouriteRoutes = require('./routes/favourite');
const productAddressRoutes = require('./routes/product-address');
const pointRoutes = require('./routes/pick-up-point');
const categoryRoutes = require('./routes/category');
const productCategoryRoutes = require('./routes/productCategory');
const organizationInfoRoutes = require('./routes/organization-info');

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
const keys = require('./config/keys');
const app = express();

mongoose.connect(keys.mongoURI)
    .then(() => console.log('MongoDB connected'))
    .catch(error => console.log(error));

app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/api/auth', authRoutes);
app.use('/api/order', orderRoutes);
app.use('/api/product', productRoutes);
app.use('/api/product', productRoutes);
app.use('/api/favourite', favouriteRoutes);
app.use('/api/productAddress', productAddressRoutes);
app.use('/api/pick-up-point', pointRoutes);
app.use('/api/category', categoryRoutes);
app.use('/api/productCategory', productCategoryRoutes);
app.use('/api/organization-info', organizationInfoRoutes);

module.exports = app;
