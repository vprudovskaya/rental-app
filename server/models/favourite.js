const mongoose = require('mongoose');
const { Schema } = mongoose;

const favouriteSchema = new Schema({
    user: {
        ref: 'users',
        type: Schema.Types.ObjectId,
        required: true
    },
    product: {
        ref: 'products',
        type: Schema.Types.ObjectId,
        required: true
    },
});

module.exports = mongoose.model('favourite', favouriteSchema);
