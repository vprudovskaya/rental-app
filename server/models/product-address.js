const mongoose = require('mongoose');
const { Schema } = mongoose;

const productAddressSchema = new Schema({
    product: {
        ref: 'products',
        type: Schema.Types.ObjectId,
        required: true
    },
    point: {
        ref: 'pickUpPoints',
        type: Schema.Types.ObjectId,
        required: true
    }
});

module.exports = mongoose.model('productAddress', productAddressSchema);
