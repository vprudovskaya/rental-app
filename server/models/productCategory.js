const mongoose = require('mongoose');
const { Schema } = mongoose;

const productCategorySchema = new Schema({
    product: {
        ref: 'product',
        type: Schema.Types.ObjectId,
        required: true
    },
    category: {
        ref: 'categories',
        type: Schema.Types.ObjectId,
        required: true
    },
});

module.exports = mongoose.model('productCategory', productCategorySchema);
