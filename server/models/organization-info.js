const mongoose = require('mongoose');
const { Schema } = mongoose;

const OrganizationInfoSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    phoneNumbers: [
        {
            type: String,
            required: true
        }
    ],
});

module.exports = mongoose.model('Info', OrganizationInfoSchema);
