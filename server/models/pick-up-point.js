const mongoose = require('mongoose');
const { Schema } = mongoose;

const pointSchema = new Schema({
    address: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('pickUpPoint', pointSchema);
