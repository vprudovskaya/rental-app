const mongoose = require('mongoose');
const { Schema } = mongoose;

const orderSchema = new Schema({
    user: {
        ref: 'users',
        type: Schema.Types.ObjectId,
        required: true
    },
    date: {
        type: Date,
        default: Date.now()
    },
    product: {
        ref: 'products',
        type: Schema.Types.ObjectId,
        required: true
    },
    cost: {
        type: Number,
        required: true
    },
    period: {
        type: Number,
        required: true
    },
    point: {
        ref: 'pickuppoints',
        type: Schema.Types.ObjectId,
        required: true
    }
});

module.exports = mongoose.model('order', orderSchema);
