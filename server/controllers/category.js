const category = require('../models/category');

module.exports.create = async function (req, res) {
    try {
        const newCategory = await new category({
            name: req.body.name,
            feature: req.body.feature
        }).save();
        res.status(201).json(newCategory);
    } catch (e) {
        console.log(e);
    }
}

module.exports.getAll = async function (req, res) {
    try {
        category.find({}, null, function (err, docs) {
            res.status(200).send(docs);
        });
    } catch (e) {
        console.log(e);
    }
}

