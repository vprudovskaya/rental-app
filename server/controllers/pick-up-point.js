const point = require('../models/pick-up-point');

module.exports.create = async function(req, res) {
    try {
        const newPoint = await new point({
            address: req.body.address
        }).save();
        res.status(201).json(newPoint);
    } catch (e) {
        console.log(e);
    }
}

module.exports.getAll = async function(req, res) {
    try {
        point.find({}, null, function (err, docs) {
            res.status(200).send(docs);
        });
    } catch (e) {
        console.log(e);
    }
}
