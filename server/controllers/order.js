const order = require('../models/order');

module.exports.getAllById = async function (req, res) {
    try {
        await order.find({user: req.query.id}).populate([{path: "product", model: "product"}, {path: "point", model: "pickUpPoint"}])
            .exec(function (err, result) {
            res.status(200).send(result);
        });
    } catch (e) {
        console.log(e);
    }
}

module.exports.create = async function (req, res) {
    try {
        const newOrder = await new order({
            user: req.body.userId,
            product: req.body.product,
            cost: req.body.cost,
            period: req.body.period,
            point: req.body.point
        }).save();
        newOrder.populate([{path: "product", model: "product"}, {path: "point", model: "pickUpPoint"}],
            (err, result) => {
                res.status(201).json(result);
            });
    } catch (e) {
        console.log(e);
    }
}

