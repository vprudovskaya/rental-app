const favourite = require('../models/favourite');
const product = require('../models/product');

module.exports.setFavourite = async function (req, res) {
    try {
        const newFavourite = await new favourite({
            user: req.body.userId,
            product: req.body.productId
        }).save();
        res.status(201).json(newFavourite);
    } catch (e) {
        console.log(e);
    }
}

module.exports.deleteFavourite = async function (req, res) {
    try {
        await favourite.remove({user: req.query.userId, product: req.query.productId});
        res.status(200).send();
    } catch (e) {
        console.log(e);
    }
}

module.exports.checkFavourite = async function (req, res) {
    try {
        const favouriteNote = await favourite.findOne({user: req.query.userId, product: req.query.productId});
        if (favouriteNote) {
            res.status(200).send(true);
        } else {
            res.status(200).send(false);
        }
    } catch (e) {
        console.log(e);
    }
}

module.exports.getAllById = async function (req, res) {
    try {
        await favourite.find({user: req.params.id})
            .populate({path: 'product', model: 'product'})
            .exec((err, result) => {
                res.status(200).send(result);
            });
    } catch (e) {
        console.log(e);
    }
}
