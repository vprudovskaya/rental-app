const product = require('../models/product');

module.exports.create = async function (req, res) {
    try {
        const newProduct = await new product({
            name: req.body.name,
            features: req.body.features,
            price: req.body.price,
            address: req.body.address
        }).save();
        res.status(201).json(product);
    } catch (e) {
        console.log(e);
    }
}

module.exports.getById = async function (req, res) {
    try {
        const foundProduct = await product.findById(req.params.id);
        res.status(200).json(foundProduct);
    } catch (e) {
        console.log(e);
    }
}

module.exports.getAll = async function (req, res) {
    try {
        product.find({}, null, function (err, docs) {
            res.status(200).send(docs);
        });
    } catch (e) {
        console.log(e);
    }
}
