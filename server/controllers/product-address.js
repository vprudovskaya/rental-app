const productAddress = require('../models/product-address');

module.exports.add = async function(req, res) {
    try {
        const newProductAddress = await new productAddress({
            product: req.body.product,
            point: req.body.point
        }).save();
        res.status(201).json(newProductAddress);
    } catch (e) {
        console.log(e);
    }
}

module.exports.getAllByProductId = async function(req, res) {
    try {
        await productAddress.find({product: req.params.id})
            .populate({path: 'point', model: 'pickUpPoint'})
            .exec((err, results) => {
                res.status(200).send(results);
            });
    } catch (e) {
        console.log(e);
    }
}
