const bcrypt = require('bcryptjs');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const keys = require('../config/keys');

module.exports.login = async function (req, res) {
    const candidate = await User.findOne({email: req.body.email});
    if (candidate) {
        const passwordResult = bcrypt.compareSync(req.body.password, candidate.password);
        if (passwordResult) {
            const token = jwt.sign({
                    email: candidate.email,
                    userId: candidate._id,
                    name: candidate.name,
                    phoneNumber: candidate.phoneNumber
                },
                keys.jwt, {expiresIn: 60 * 60});
            res.status(200).json({
                token: `Bearer ${token}`
            });
        } else {
            res.status(401);
        }
    } else {
        res.status(404);
    }
};

module.exports.register = async function (req, res) {
    const candidate = await User.findOne({email: req.body.email});
    if (candidate) {
        res.status(409).send();
    } else {
        const salt = bcrypt.genSaltSync(10);
        const user = new User({
            name: req.body.name,
            email: req.body.email,
            phoneNumber: req.body.phoneNumber,
            password: bcrypt.hashSync(req.body.password, salt)
        });
        try {
            await user.save();
            res.status(201).json(user);
        } catch (e) {
        }
    }
};

module.exports.update = async function (req, res) {
    const candidate = await User.findOne({email: req.body.user.email});
    if ((req.body.email != req.body.user.email) && candidate) {
        res.status(409).send();
    } else {
        try {
            await User.updateOne(
                {email: req.body.email},
                {
                    $set: {
                        name: req.body.user.name,
                        email: req.body.user.email,
                        phoneNumber: req.body.user.phoneNumber
                    }
                },
                null,
                () => {
                    const token = jwt.sign({
                            email: req.body.user.email,
                            userId: req.body.id,
                            name: req.body.user.name,
                            phoneNumber: req.body.user.phoneNumber
                        },
                        keys.jwt, {expiresIn: 60 * 60});
                    res.status(200).json({
                        token: `Bearer ${token}`
                    });
                });
        } catch (e) {

        }
    }

    // if (req.body.email == req.body.user.email) {
    //     try {
    //         await User.updateOne(
    //             {email: req.body.email},
    //             {
    //                 $set: {
    //                     name: req.body.user.name,
    //                     email: req.body.user.email,
    //                     phoneNumber: req.body.user.phoneNumber
    //                 }
    //             },
    //             null,
    //             () => {
    //                 const token = jwt.sign({
    //                         email: req.body.user.email,
    //                         userId: req.body.id,
    //                         name: req.body.user.name,
    //                         phoneNumber: req.body.user.phoneNumber
    //                     },
    //                     keys.jwt, {expiresIn: 60 * 60});
    //                 res.status(200).json({
    //                     token: `Bearer ${token}`
    //                 });
    //             });
    //     } catch (e) {
    //
    //     }
    // } else {
    //     const candidate = await User.findOne({email: req.body.user.email});
    //     if (candidate) {
    //         res.status(409).send();
    //     } else {
    //         try {
    //             await User.updateOne(
    //                 {email: req.body.email},
    //                 {
    //                     $set: {
    //                         name: req.body.user.name,
    //                         email: req.body.user.email,
    //                         phoneNumber: req.body.user.phoneNumber
    //                     }
    //                 },
    //                 null,
    //                 () => {
    //                     const token = jwt.sign({
    //                             email: req.body.user.email,
    //                             userId: req.body.id,
    //                             name: req.body.user.name,
    //                             phoneNumber: req.body.user.phoneNumber
    //                         },
    //                         keys.jwt, {expiresIn: 60 * 60});
    //                     res.status(200).json({
    //                         token: `Bearer ${token}`
    //                     });
    //                 });
    //         } catch (e) {
    //
    //         }
    //     }
    // }
}
