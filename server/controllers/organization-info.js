const Info = require('../models/organization-info');

module.exports.set = async function (req, res) {
    try {
        const newInfo = await new Info({
            name: req.body.name,
            phoneNumbers: req.body.phoneNumbers
        }).save();
        res.status(201).json(newInfo);
    } catch (e) {
        console.log(e);
    }
}

module.exports.get = async function (req, res) {
    try {
        Info.find({}, null, function (err, docs) {
            res.status(200).send(docs);
        });
    } catch (e) {
        console.log(e);
    }
}
