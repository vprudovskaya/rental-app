const productCategory = require('../models/productCategory');
const category = require('../models/category');
const product = require('../models/product');

module.exports.post = async function (req, res) {
    try {
        const newProductCategory = await new productCategory({
            product: req.body.productId,
            category: req.body.categoryId
        }).save();
        res.status(201).json(newProductCategory);
    } catch (e) {
        console.log(e);
    }
}

module.exports.getProductsByCategoryName = async function(req, res) {
    try {
        const foundCategory = await category.findOne({name: req.params.categoryName});
        await productCategory.find({category: foundCategory._id})
            .populate({path: 'product', model: 'product'})
            .exec((err, results) => {
                res.status(200).send(results);
            });
    } catch (e) {
        console.log(e);
    }
}
