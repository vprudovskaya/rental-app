import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: 'app-order-info',
  templateUrl: './order-info.component.html',
  styleUrls: ['./order-info.component.less']
})
export class OrderInfoComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.data.order._id = this.data.order._id.substr(0, 8);
    this.data.order.DMY = this.data.order.date.substr(8, 2) + '.'
      + this.data.order.date.substr(5, 2) + '.'
      + this.data.order.date.substr(0, 4);
  }
}
