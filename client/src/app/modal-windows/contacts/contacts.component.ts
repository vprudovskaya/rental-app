import { Component, OnInit } from '@angular/core';
import {DataService} from "../../services/data.service";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.less']
})
export class ContactsComponent {
  public phoneNumbers = this.dataService.getOrganizationInfo()
    .pipe(
      map((data) => {
          const info = Object.values(data);
          return info[0].phoneNumbers;
      })
    );

  public points = this.dataService.getAllPoints()
    .pipe(
      map((data) => {
        const pointsValues = Object.values(data);
        return pointsValues;
      })
    );

  constructor(private dataService: DataService) { }

}
