import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {RoutingService} from "../../services/routing.service";


@Component({
  selector: 'app-order-processed-modal',
  templateUrl: './order-processed-modal.component.html',
  styleUrls: ['./order-processed-modal.component.less']
})
export class OrderProcessedModalComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<OrderProcessedModalComponent>,
              private routingService: RoutingService) {
    this.data.orderId = this.data.orderId.substr(0, 8);
  }

  public _goToCabinet(): void {
    this.routingService.goToCabinet();
    this.dialogRef.close();
  }
}
