import { Component, OnInit } from '@angular/core';
import {DataService} from "../../services/data.service";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-pick-up-points',
  templateUrl: './pick-up-points.component.html',
  styleUrls: ['./pick-up-points.component.less']
})
export class PickUpPointsComponent implements OnInit {
  public points$ = this.productService.getAllPoints()
    .pipe(
      map((points) => {
        return Object.values(points);
      })
  );

  constructor(private productService: DataService) { }

  ngOnInit(): void {
  }

}
