import {Component} from '@angular/core';
import {RoutingService} from "../../services/routing.service";

@Component({
  selector: 'app-login-error-dialog',
  templateUrl: './login-error-dialog.component.html',
  styleUrls: ['./login-error-dialog.component.less']
})
export class LoginErrorDialogComponent {

  constructor(private router: RoutingService) {
  }

  public _goToRegPage(): void {
    this.router.goToRegPage();
  }
}
