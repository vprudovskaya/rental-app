import {Component} from '@angular/core';
import {RoutingService} from "../../services/routing.service";

@Component({
  selector: 'app-registration-error',
  templateUrl: './registration-error.component.html',
  styleUrls: ['./registration-error.component.less']
})
export class RegistrationErrorComponent {

  constructor(private router: RoutingService) {
  }

  public _goToLoginPage(): void {
    this.router.goToLoginPage();
  }
}
