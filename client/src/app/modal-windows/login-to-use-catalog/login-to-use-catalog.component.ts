import { Component, OnInit } from '@angular/core';
import {RoutingService} from "../../services/routing.service";

@Component({
  selector: 'app-login-to-use-catalog',
  templateUrl: './login-to-use-catalog.component.html',
  styleUrls: ['./login-to-use-catalog.component.less']
})
export class LoginToUseCatalogComponent {

  constructor(private router: RoutingService) { }

  public _goToRegPage(): void {
    this.router.goToRegPage();
  }

  public _goToLoginPage(): void {
    this.router.goToLoginPage();
  }

}
