import {Component, Inject} from '@angular/core';
import {RoutingService} from "../../services/routing.service";
import {AuthService} from "../../services/auth.service";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {logUser} from "../../shared/models/log.user.model";

@Component({
  selector: 'app-reg-modal-dialog',
  templateUrl: './reg-modal-dialog.component.html',
  styleUrls: ['./reg-modal-dialog.component.less']
})
export class RegModalDialogComponent {
  public user: logUser = {
    email: '',
    password: ''
  };

  constructor(private router: RoutingService,
              private auth: AuthService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.user.email = this.data.user.email;
    this.user.password = this.data.user.password;
  }

  public _goStartPage() {
    this.router.goToStartPage();
  }

  public _login() {
    this.auth.login(this.user).subscribe(() => {
      this.router.goToCatalog();
    })
  }
}
