import {Component, ViewEncapsulation} from '@angular/core';
import {DataService} from "../../services/data.service";
import {map} from "rxjs/operators";
import {MatDialogRef} from "@angular/material/dialog";
import {ThemePalette} from '@angular/material/core';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class FiltersComponent {
  public selectedCategory!: any;
  public categories$ = this.productService.getAllCategories()
    .pipe(
      map((categories) => {
        return Object.values(categories);
      })
    );

  constructor(private productService: DataService,
              private dialogRef: MatDialogRef<FiltersComponent>) {
  }

  public _closeWithoutCategory(): void {
    this.dialogRef.close('all');
  }

  public _changeCategory(category: any): void {
    this.selectedCategory = category;
  }

  public _close() {
    this.dialogRef.close(this.selectedCategory);
  }
}
