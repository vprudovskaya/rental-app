import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {MatDialogRef} from "@angular/material/dialog";
import {UserInfo} from "../../shared/models/user.info";

@Component({
  selector: 'app-edit-user-info',
  templateUrl: './edit-user-info.component.html',
  styleUrls: ['./edit-user-info.component.less']
})
export class EditUserInfoComponent implements OnInit {
  public editForm!: FormGroup;
  public userExists: boolean = false;

  constructor(private auth: AuthService,
              private dialogRef: MatDialogRef<EditUserInfoComponent>) {
  }

  ngOnInit(): void {
    this.editForm = new FormGroup({
      name: new FormControl(this.auth.getDecodedName(), [Validators.required]),
      email: new FormControl(this.auth.getDecodedEmail(), [Validators.required, Validators.email]),
      phoneNumber: new FormControl(this.auth.getDecodedPhoneNumber(), [Validators.required])
    });
  }

  public _onSubmit(): void {
    const userData: UserInfo = {
      name: this.auth.getDecodedName(),
      email: this.auth.getDecodedEmail(),
      phoneNumber: this.auth.getDecodedPhoneNumber()
    }
    if (userData.name != this.editForm.controls.name.value
      || userData.email != this.editForm.controls.email.value
      || userData.phoneNumber != this.editForm.controls.phoneNumber.value) {
      this.auth.editUserInfo(this.editForm.value).subscribe(
        () => {
          this.userExists = false;
          this.dialogRef.close();
        },
        (error) => {
          this.userExists = true;
        });
    } else {
      this.dialogRef.close();
    }
  }
}
