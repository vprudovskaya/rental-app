import {Product} from "./models/product.model";

export const productsArray: Product[] = [
  {
    id: '6037a3c02805c14df8228770',
    name: 'Электросамокат R999',
    features: 'Для тех, кто в тренде',
    price: 2500,
  },
  {
    id: '6034fac0f02d6418345d21f1',
    name: 'Велосипед NN8000',
    features: 'Отлично подходит для поездок по городу',
    price: 3400,
  },
  {
    id: '6037a3eb2805c14df8228771',
    name: 'Самокат F003',
    features: 'Очень удобный руль!',
    price: 1700,
  },
  {
    id: '6037a3742805c14df822876f',
    name: 'Ролики I77',
    features: 'Для самых маленьких',
    price: 1200,
  },
  {
    id: '6037a46b2805c14df8228773',
    name: 'Скейтборд Y666',
    features: 'Для тех, кто хочет вспомнить детство',
    price: 2000,
  }
]
