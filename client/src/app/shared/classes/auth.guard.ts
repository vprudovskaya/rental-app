import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import {Observable, of} from "rxjs";
import {Injectable} from "@angular/core";
import {AuthService} from "../../services/auth.service";
import {MatDialog} from "@angular/material/dialog";
import {LoginErrorDialogComponent} from "../../modal-windows/login-error-dialog/login-error-dialog.component";
import {LoginToUseCatalogComponent} from "../../modal-windows/login-to-use-catalog/login-to-use-catalog.component";
import {RegModalDialogComponent} from "../../modal-windows/reg-modal-dialog/reg-modal-dialog.component";
import {RoutingService} from "../../services/routing.service";

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(private auth: AuthService,
              private router: RoutingService,
              private dialog: MatDialog) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    if (this.auth.isAuthenticated()) {
      return of(true);
    } else {
      this.showDialog();
      return of(false);
    }
  }

  private showDialog(): void {
    this.dialog.open(LoginToUseCatalogComponent, {
      height: '442px',
      width: '700px',
    });
  }
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(childRoute, state);
  }
}
