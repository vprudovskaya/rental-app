export interface Product {
  id: string;
  name: string;
  features: string;
  price: number;
}
