export interface regUser {
  name: string;
  email: string;
  phoneNumber: string;
  password: string;
}
