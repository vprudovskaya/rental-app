export interface UserInfo {
  email: string;
  name: string;
  phoneNumber: string;
}
