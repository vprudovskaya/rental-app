import {NgModule} from "@angular/core";
import {FooterComponent} from "../../footer/footer.component";
import {ProductComponent} from "../../after-auth/product/product.component";
import {MaterialModule} from "./material.module";
import {CommonModule} from "@angular/common";
import {AngularYandexMapsModule, YaConfig} from 'angular8-yandex-maps';

const mapConfig: YaConfig = {
  apikey: 'f2a395cf-1fa9-4741-b255-06fc71eb0f81',
  lang: 'ru_RU',
};

@NgModule({
  declarations: [
    FooterComponent,
    ProductComponent
  ],
  imports: [
    MaterialModule,
    CommonModule,
    AngularYandexMapsModule.forRoot(mapConfig)
  ],
  exports: [
    MaterialModule,
    AngularYandexMapsModule,
    FooterComponent,
    ProductComponent
  ]
})

export class SharedModule {
}
