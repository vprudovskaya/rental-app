import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {StartPageComponent} from "./start-page/start-page.component";
import {RegistrationComponent} from "./registration/registration.component";
import {AuthorizationComponent} from "./authorization/authorization.component";
import {AuthGuard} from "./shared/classes/auth.guard";

const routes: Routes = [
  {path: '', component: StartPageComponent},
  {path: 'registration', component: RegistrationComponent},
  {path: 'authorization', component: AuthorizationComponent},
  {path: 'authorized', loadChildren: () => import('./after-auth/using-app.module').then(m => m.UsingAppModule), canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
