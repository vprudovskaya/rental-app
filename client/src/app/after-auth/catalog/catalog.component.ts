import {Component} from '@angular/core';
import {DataService} from "../../services/data.service";
import {map} from "rxjs/operators";
import {MatDialog} from "@angular/material/dialog";
import {FiltersComponent} from "../../modal-windows/filters/filters.component";
import {fadeOutOnLeaveAnimation, fadeInOnEnterAnimation} from "angular-animations";

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.less'],
  animations: [
    fadeInOnEnterAnimation({duration: 300}),
    fadeOutOnLeaveAnimation({duration: 300})
  ]
})
export class CatalogComponent {
  public buttonType: string = 'favorite_border';
  public selectedCategory: any;
  public products$ = this.productService.getAllProducts()
    .pipe(
      map((products) => {
        return Object.values(products);
      })
    );

  constructor(private productService: DataService,
              private dialog: MatDialog) {
  }

  public _showDialog(): void {
    const dialogRef = this.dialog.open(FiltersComponent, {
      width: '1245px',
      height: '260px',
    });

    dialogRef.afterClosed().subscribe((category) => {
        if (category && (category != 'all')) {
          this.products$ = this.productService.getProductsByCategoryName(category.name)
            .pipe(
              map((products) => {
                let productsValues = Object.values(products);
                for (let i = 0; i < productsValues.length; i++) {
                  productsValues[i] = productsValues[i].product;
                }
                return productsValues;
              })
            );
        } else if (category == 'all') {
          this.products$ = this.productService.getAllProducts()
            .pipe(
              map((products) => {
                return Object.values(products);
              })
            );
        }
      }
    );
  }
}
