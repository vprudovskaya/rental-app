import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {DataService} from "../../services/data.service";
import {map} from "rxjs/operators";
import {MatDialog} from "@angular/material/dialog";
import {OrderInfoComponent} from "../../modal-windows/order-info/order-info.component";
import {fadeOutOnLeaveAnimation} from "angular-animations";
import {EditUserInfoComponent} from "../../modal-windows/edit-user-info/edit-user-info.component";

@Component({
  selector: 'app-cabinet',
  templateUrl: './cabinet.component.html',
  styleUrls: ['./cabinet.component.less'],
  animations: [
    fadeOutOnLeaveAnimation({duration: 500})
  ]
})
export class CabinetComponent implements OnInit {
  public userName!: string;
  public userEmail!: string;
  public userPhoneNumber!: string;
  public buttonText: string = 'каталог';
  public buttonType: string = 'clear';
  public favourites!: any[];
  public orders$ = this.productService.getOrders()
    .pipe(
      map((orders) => {
        let orderValues = Object.values(orders);
        for (let i = 0; i < orderValues.length; i++) {
          orderValues[i]._id = orderValues[i]._id.substr(0, 8);
        }
        for (let i = 0; i < orderValues.length; i++) {
          orderValues[i].DMY = orderValues[i].date.substr(8, 2) + '.'
            + orderValues[i].date.substr(5, 2) + '.'
            + orderValues[i].date.substr(0, 4);
        }
        return orderValues;
      }),
    );

  constructor(private auth: AuthService,
              private productService: DataService,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.productService.getAllFavouritesByUserId().subscribe((result) => {
      this.favourites = Object.values(result);
    });

    this.userName = this.auth.getDecodedName();
    this.userEmail = this.auth.getDecodedEmail();
    this.userPhoneNumber = this.auth.getDecodedPhoneNumber();
    this.auth.setUserName(this.userName);
  }

  public _openEditForm(): void {
    const dialogRef = this.dialog.open(EditUserInfoComponent, {
      height: '550px',
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(() => {
      this.userName = this.auth.getDecodedName();
      this.userEmail = this.auth.getDecodedEmail();
      this.userPhoneNumber = this.auth.getDecodedPhoneNumber();
      }
    );
  }

  public _deleteFavourite(product: any): void {
    this.favourites = this.favourites.filter((favourites) => {
      return favourites.product != product;
    });
  }

  _showOrderInfo(order: any): void {
    this.dialog.open(OrderInfoComponent, {
      height: '442px',
      width: '700px',
      data: {
        order: order
      }
    });
  }
}
