import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";
import {DataService} from "../../services/data.service";
import {RoutingService} from "../../services/routing.service";

@Component({
  selector: 'app-small-product-card',
  templateUrl: './small-product-card.component.html',
  styleUrls: ['./small-product-card.component.less']
})
export class SmallProductCardComponent implements OnInit {
  @Input() public product: any;
  @Input() public buttonType!: string;
  @Output() public delete: EventEmitter<string> = new EventEmitter();

  constructor(private router: RoutingService,
              private productService: DataService) { }

  ngOnInit(): void {
    if (this.buttonType != 'clear') {
      this.productService.checkFavourite(this.product._id).subscribe((res) => {
        if (res) {
          this.buttonType = 'favorite';
        } else {
          this.buttonType = 'favorite_border';
        }
      });
    }
  }

  public _openProductCard(): void {
    this.router.openProductCard(this.product._id);
  }

  public _addFavouriteOrRemove(): void {
    if (this.buttonType == 'favorite_border') {
      this.productService.setFavourite(this.product._id).subscribe(() => {
        this.buttonType = 'favorite';
      });
    } else {
      if (this.buttonType == 'favorite') {
        this.productService.deleteFavourite(this.product._id).subscribe();
        this.buttonType = 'favorite_border';
      } else {
        this.productService.deleteFavourite(this.product._id).subscribe();
        this.delete.emit(this.product);
      }
    }
  }
}
