import {RouterModule, Routes} from "@angular/router";
import {CabinetComponent} from "./cabinet/cabinet.component";
import {CatalogComponent} from "./catalog/catalog.component";
import {ProductComponent} from "./product/product.component";
import {AfterAuthComponent} from "./after-auth-component/after-auth.component";
import {NgModule} from "@angular/core";

const childrenRoutes: Routes = [
  {path: 'cabinet', component: CabinetComponent},
  {path: 'catalog', component: CatalogComponent},
  {path: 'product/:id', component: ProductComponent}
]

const routes: Routes = [
  {path: '', component: AfterAuthComponent, children: childrenRoutes}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsingAppRoutingModule { }
