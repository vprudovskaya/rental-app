import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {DataService} from "../../services/data.service";
import {Product} from "../../shared/models/product.model";
import {AuthService} from "../../services/auth.service";
import {MatDialog} from "@angular/material/dialog";
import {OrderProcessedModalComponent} from "../../modal-windows/order-processed-modal/order-processed-modal.component";
import {MapComponent} from "../../map/map.component";
import {RoutingService} from "../../services/routing.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.less']
})
export class ProductComponent implements OnInit {
  public productId!: string;
  public product!: Product;
  public buttonText: string = 'каталог';
  public buttonFavouriteText!: string;
  public addresses!: any[];
  public selectedAddress!: any;
  public selectedPeriod!: number;
  public possiblePeriods: number[] = [1, 2, 4, 24];
  @Input() inputProduct!: Product;
  @Input() isBookingAvailable: boolean = true;

  constructor(private activateRoute: ActivatedRoute,
              private http: HttpClient,
              private productService: DataService,
              private authService: AuthService,
              private dialog: MatDialog,
              private router: RoutingService) { }

  ngOnInit(): void {
    this.productId = this.activateRoute.snapshot.params['id'];
    if (this.productId) {
      this.getProductAddresses(this.productId);
      this.productService.getProductById(this.productId).subscribe(
        (product) => {
          this.product = product;
        }
      );
      this.productService.checkFavourite(this.productId).subscribe((res) => {
        if (res) {
          this.buttonFavouriteText = 'в избранном';
        } else {
          this.buttonFavouriteText = 'добавить в избранное';
        }
      });
    } else if (this.inputProduct) {
      this.product = this.inputProduct;
      this.getProductAddresses(this.product.id);
    }
  }

  private getProductAddresses(productId: string): void {
    this.productService.getAddressesByProductId(productId).subscribe((results) => {
      this.addresses = Object.values(results);
      for (let i = 0; i <= this.addresses.length; i++) {
        this.addresses[i] = this.addresses[i].point;
      }
    });
  }

  public _addToFavourite(): void {
    if (this.buttonFavouriteText != 'в избранном') {
      this.productService.setFavourite(this.productId).subscribe();
      this.buttonFavouriteText = 'в избранном';
    } else {
      this.productService.deleteFavourite(this.productId).subscribe();
      this.buttonFavouriteText = 'добавить в избранное';
    }
  }

  private countOrderCost(): number {
    return this.product.price * this.selectedPeriod;
  }

  public _bookProduct(): void {
    this.productService.createOrder(this.productId, this.countOrderCost(), this.selectedPeriod, this.selectedAddress._id).subscribe((order) => {
      this.showDialog(order);
    });
  }

  public _changePeriod(period: number): void {
    this.selectedPeriod = period;
  }

  public _changeAddress(address: any): void {
    this.selectedAddress = address;
  }

  private showDialog(order: any): void {
    this.dialog.open(OrderProcessedModalComponent, {
      height: '442px',
      width: '700px',
      data: {
        product: order.product,
        address: order.point.address,
        period: order.period,
        cost: order.cost,
        orderId: order._id
      }
    });
  }

  public _openMap(): void {
    const dialogRef = this.dialog.open(MapComponent, {
      height: '900px',
      width: '1290px',
      data: {
        addresses: this.addresses
      }
    });

    dialogRef.afterClosed().subscribe((address) => {
      this._changeAddress(address);
    });
  }

  public _goRegPage(): void {
    this.router.goToRegPage();
  }
}
