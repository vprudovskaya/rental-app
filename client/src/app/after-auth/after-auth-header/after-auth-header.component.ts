import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import {RoutingService} from "../../services/routing.service";

@Component({
  selector: 'app-after-auth-header',
  templateUrl: './after-auth-header.component.html',
  styleUrls: ['./after-auth-header.component.less']
})
export class AfterAuthHeaderComponent {
  public userName!: string;

  constructor(private router: RoutingService, private auth: AuthService) { }

  ngDoCheck(): void {
    this.userName = this.auth.getUserName();
  }

  public _toCabinet():void {
    this.router.goToCabinet();
  }

  public _exit(): void {
    localStorage.removeItem('auth-token');
    this.router.goToLoginPage();
  }

  public _goToCatalog() {
    this.router.goToCatalog();
  }
}
