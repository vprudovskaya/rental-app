import {RouterModule} from "@angular/router";
import {CabinetComponent} from "./cabinet/cabinet.component";
import {CatalogComponent} from "./catalog/catalog.component";
import {NgModule} from "@angular/core";
import {AfterAuthHeaderComponent} from "./after-auth-header/after-auth-header.component";
import {LoginErrorDialogComponent} from "../modal-windows/login-error-dialog/login-error-dialog.component";
import {OrderProcessedModalComponent} from "../modal-windows/order-processed-modal/order-processed-modal.component";
import {SmallProductCardComponent} from "./small-product-card/small-product-card.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MaterialModule} from "../shared/modules/material.module";
import {DataService} from "../services/data.service";
import {AfterAuthComponent} from "./after-auth-component/after-auth.component";
import {CommonModule} from "@angular/common";
import {UsingAppRoutingModule} from "./using-app-routing.module";
import {SharedModule} from "../shared/modules/shared.module";
import {ProductComponent} from "./product/product.component";

@NgModule({
  declarations: [
    AfterAuthComponent,
    AfterAuthHeaderComponent,
    CabinetComponent,
    CatalogComponent,
    LoginErrorDialogComponent,
    OrderProcessedModalComponent,
    SmallProductCardComponent
  ],
  imports: [
    SharedModule,
    UsingAppRoutingModule,
    RouterModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    DataService
  ],
  exports: [
    AfterAuthHeaderComponent,
  ]
})

export class UsingAppModule { }
