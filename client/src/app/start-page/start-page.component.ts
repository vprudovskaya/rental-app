import {Component, OnInit} from '@angular/core';
import {RoutingService} from "../services/routing.service";
import {DataService} from "../services/data.service";

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.less']
})
export class StartPageComponent implements OnInit{
  public organizationName: string = '';

  constructor(private router: RoutingService,
              private dataService: DataService) {
  }

  ngOnInit() {
    this.dataService.getOrganizationInfo().subscribe((result) => {
      const info = Object.values(result);
      this.organizationName = info[0].name;
    })
  }

  public _goToRegPage(): void {
    this.router.goToRegPage();
  }
}
