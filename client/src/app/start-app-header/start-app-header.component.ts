import { Component, OnInit } from '@angular/core';
import {RoutingService} from "../services/routing.service";

@Component({
  selector: 'app-start-app-header',
  templateUrl: './start-app-header.component.html',
  styleUrls: ['./start-app-header.component.less']
})
export class StartAppHeaderComponent {
  constructor(private router: RoutingService) {
  }

  public _goToLoginPage(): void {
    this.router.goToLoginPage();
  }

  public _goToRegPage(): void {
    this.router.goToRegPage();
  }
}
