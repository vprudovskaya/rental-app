import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RegistrationComponent} from './registration/registration.component';
import {AuthorizationComponent} from './authorization/authorization.component';
import {StartPageComponent} from './start-page/start-page.component';
import {StartAppHeaderComponent} from './start-app-header/start-app-header.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AuthService} from "./services/auth.service";
import {AuthGuard} from "./shared/classes/auth.guard";
import {JwtHelperService, JwtModule} from "@auth0/angular-jwt";
import {RegModalDialogComponent} from './modal-windows/reg-modal-dialog/reg-modal-dialog.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TokenInterceptor} from "./shared/classes/token.interceptor";
import {SharedModule} from "./shared/modules/shared.module";
import {DataService} from "./services/data.service";
import {RoutingService} from "./services/routing.service";
import {PickUpPointsComponent} from './modal-windows/pick-up-points/pick-up-points.component';
import { FiltersComponent } from './modal-windows/filters/filters.component';
import { LoginToUseCatalogComponent } from './modal-windows/login-to-use-catalog/login-to-use-catalog.component';
import {AgmCoreModule} from "@agm/core";
import { MapComponent } from './map/map.component';
import { OrderInfoComponent } from './modal-windows/order-info/order-info.component';
import { CardSliderComponentComponent } from './card-slider-component/card-slider-component.component';
import { RegistrationErrorComponent } from './modal-windows/registration-error/registration-error.component';
import { ContactsComponent } from './modal-windows/contacts/contacts.component';
import { EditUserInfoComponent } from './modal-windows/edit-user-info/edit-user-info.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    AuthorizationComponent,
    StartPageComponent,
    StartAppHeaderComponent,
    RegModalDialogComponent,
    PickUpPointsComponent,
    FiltersComponent,
    LoginToUseCatalogComponent,
    MapComponent,
    OrderInfoComponent,
    CardSliderComponentComponent,
    RegistrationErrorComponent,
    ContactsComponent,
    EditUserInfoComponent
  ],
    imports: [
        SharedModule,
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        JwtModule,
        AgmCoreModule
    ],
  providers: [
    RoutingService,
    AuthGuard,
    AuthService,
    JwtHelperService,
    DataService,
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: TokenInterceptor
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

