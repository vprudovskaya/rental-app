import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../services/auth.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {RegModalDialogComponent} from "../modal-windows/reg-modal-dialog/reg-modal-dialog.component";
import {RoutingService} from "../services/routing.service";
import {RegistrationErrorComponent} from "../modal-windows/registration-error/registration-error.component";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.less'],
  providers: [MatDialog]
})
export class RegistrationComponent implements OnInit {
  public regForm!: FormGroup;

  constructor(private auth: AuthService,
              private router: RoutingService,
              private activatedRoute: ActivatedRoute,
              private dialog: MatDialog) { }

  ngOnInit(): void {
    this.regForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      phoneNumber: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required, Validators.minLength(4)])
    });
  }

  public _goToStartPage() {
    this.router.goToStartPage();
  }

  public _onSubmit(): void {
    this.regForm.disable();
    this.auth.register(this.regForm.value).subscribe(
      () => {
        this.showDialog();
      },
      error => {
        this.showErrorDialog();
        this.regForm.enable();
      }
    );
  }

  private showErrorDialog(): void {
    this.dialog.open(RegistrationErrorComponent, {
      height: '442px',
      width: '700px',
    });
  }

  private showDialog(): void {
    this.dialog.open(RegModalDialogComponent, {
      height: '442px',
      width: '700px',
      data: {
        user: this.regForm.value
      }
    });
  }
}

