import {Component} from '@angular/core';
import {RoutingService} from "../services/routing.service";
import {MatDialog} from "@angular/material/dialog";
import {MapComponent} from "../map/map.component";
import {ContactsComponent} from "../modal-windows/contacts/contacts.component";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less']
})
export class FooterComponent {
  constructor(private router: RoutingService,
              private dialog: MatDialog) {
  }

  public _goToCatalog() {
    this.router.goToCatalog();
  }

  public _showMap(): void {
    this.dialog.open(MapComponent, {
      height: '780px',
      width: '1290px',
      panelClass: 'trend-dialog',
    });
  }

  public _showContacts(): void {
    this.dialog.open(ContactsComponent, {
      height: '442px',
      width: '700px',
    });
  }
}
