import {Injectable} from "@angular/core";
import {Router} from "@angular/router";

@Injectable()
export class RoutingService {
  constructor(private router: Router) {
  }

  public goToCatalog(): void {
    this.router.navigate(['/authorized/catalog']);
  }

  public goToCabinet(): void {
    this.router.navigate(['/authorized/cabinet']);
  }

  public goToStartPage(): void {
    this.router.navigate(['/']);
  }

  public goToRegPage(): void {
    this.router.navigate(['/registration']);
  }

  public goToLoginPage(): void {
    this.router.navigate(['/authorization']);
  }

  public openProductCard(productId: string): void {
    this.router.navigateByUrl('/authorized/product/' + productId);
  }
}
