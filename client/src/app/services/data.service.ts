import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Product} from "../shared/models/product.model";
import {Observable} from "rxjs";
import {AuthService} from "./auth.service";

@Injectable()
export class DataService {

  constructor(private httpClient: HttpClient,
              private auth: AuthService) {
  }

  public getOrganizationInfo() {
    return this.httpClient.get('http://localhost:5000/api/organization-info/');
  }

  public getProductsByCategoryName(categoryName: string) {
    return this.httpClient.get('http://localhost:5000/api/productCategory/' + categoryName);
  }

  public getAllCategories() {
    return this.httpClient.get('http://localhost:5000/api/category/');
  }

  public getOrders() {
    const currentUserId = this.auth.getDecodedId();
    return this.httpClient.get('http://localhost:5000/api/order?id=' + currentUserId);
  }

  public getAllPoints() {
    return this.httpClient.get('http://localhost:5000/api/pick-up-point/');
  }

  public getAddressesByProductId(productId: string) {
    return this.httpClient.get('http://localhost:5000/api/productAddress/' + productId);
  }

  public getAllFavouritesByUserId() {
    return this.httpClient.get('http://localhost:5000/api/favourite/' + this.auth.getDecodedId());
  }

  public checkFavourite(productId: string) {
    return this.httpClient.get('http://localhost:5000/api/favourite?userId='
      + this.auth.getDecodedId() + '&productId=' + productId);
  }

  public deleteFavourite(productId: string): Observable<object> {
    return this.httpClient.delete('http://localhost:5000/api/favourite?userId='
    + this.auth.getDecodedId() + '&productId=' + productId);
  }

  public setFavourite(productId: string) {
    return this.httpClient.post('http://localhost:5000/api/favourite/', {
      userId: this.auth.getDecodedId(),
      productId: productId
    });
  }

  public getAllProducts() {
    return this.httpClient.get('http://localhost:5000/api/product/');
  }

  public getProductById(id: string): Observable<Product> {
    return this.httpClient.get<Product>('http://localhost:5000/api/product/' + id);
  }

  public createOrder(productId: string, cost: number, period: number, pointId: string) {
    const currentUserId = this.auth.getDecodedId();
    return this.httpClient.post('http://localhost:5000/api/order/', {
      userId: currentUserId,
      product: productId,
      cost: cost,
      period: period,
      point: pointId
    });
  }
}
