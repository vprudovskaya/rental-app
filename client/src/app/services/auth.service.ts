import {Injectable} from "@angular/core";
import {logUser} from "../shared/models/log.user.model";
import {regUser} from "../shared/models/reg.user.model";
import {UserInfo} from "../shared/models/user.info";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {JwtHelperService} from '@auth0/angular-jwt';


@Injectable()
export class AuthService {
  private token!: string;
  private jwtHelper = new JwtHelperService();
  public userName!: string;

  constructor(private httpClient: HttpClient) {

  }

  public setUserName(name: string): void {
    this.userName = name;
  }

  public getUserName(): string {
    return this.userName;
  }

  public register(user: regUser): Observable<regUser> {
    return this.httpClient.post<regUser>('http://localhost:5000/api/auth/register', user);
  }

  public login(user: logUser): Observable<{ token: string }> {
    return this.httpClient.post<{ token: string }>('http://localhost:5000/api/auth/login', user)
      .pipe(
        tap(
          ({token}) => {
            localStorage.setItem('auth-token', token);
            this.setToken(token);
            this.userName = this.getDecodedName();
          }
        )
      );
  }

  public editUserInfo(user: UserInfo): Observable<{ token: string }> {
    return this.httpClient.patch<{ token: string }>('http://localhost:5000/api/auth/', {
      email: this.getDecodedEmail(),
      id: this.getDecodedId(),
      user: {
        email: user.email,
        name: user.name,
        phoneNumber: user.phoneNumber
      }
    }).pipe(
      tap(
        ({token}) => {
          localStorage.setItem('auth-token', token);
          this.setToken(token);
          this.userName = this.getDecodedName();
        },
        (error) => {

        }
      )
    );
  }

  public setToken(token: string): void {
    this.token = token;
  }

  public getToken(): string {
    return this.token;
  }

  public getDecodedId() {
    return this.jwtHelper.decodeToken(this.token).userId;
  }

  public getDecodedName() {
    return this.jwtHelper.decodeToken(this.token).name;
  }

  public getDecodedEmail() {
    return this.jwtHelper.decodeToken(this.token).email;
  }

  public getDecodedPhoneNumber() {
    return this.jwtHelper.decodeToken(this.token).phoneNumber;
  }

  public isAuthenticated(): boolean {
    return !!localStorage.getItem('auth-token');
  }
}
