import {Component} from '@angular/core';
import {productsArray} from "../shared/products";
import {Product} from "../shared/models/product.model";
import {animate, group, query, style, transition, trigger} from "@angular/animations";

const left = [
  query(':enter, :leave', style({width: '120%'}), {optional: true}),
  group(
    [
      query(':enter',
        [
          style({transform: 'translateX(-100%)'}),
          animate('0.4s ease-out', style({transform: 'translateX(50%)'}))
        ],
        {
          optional: true,
        }
      ),
      query(':leave',
        [
          style({transform: 'translateX(0%)'}),
          animate('0.4s ease-out', style({transform: 'translateX(100%)'}))
        ],
        {
          optional: true,
        }
      ),
    ]),
];

const right = [
  query(':enter, :leave', style({width: '100%'}), {optional: true}),
  group([
    query(':enter',
      [
        style({transform: 'translateX(100%)'}),
        animate('0.4s ease-out', style({transform: 'translateX(-50%)'}))
      ],
      {
        optional: true,
      }
    ),
    query(':leave',
      [
        style({transform: 'translateX(0%)'}),
        animate('0.4s ease-out', style({transform: 'translateX(-100%)'}))
      ],
      {
        optional: true,
      }),
  ]),
];

@Component({
  selector: 'app-card-slider-component',
  templateUrl: './card-slider-component.component.html',
  styleUrls: ['./card-slider-component.component.less'],
  animations: [
    trigger('animationSlider', [
      transition(':increment', right),
      transition(':decrement', left),
    ]),
  ]
})
export class CardSliderComponentComponent {
  public showedProductsIndex: number;
  public products: Product[] = productsArray;
  public outRight: boolean = true;
  public outLeft: boolean = false;
  public inLeft: boolean = false;
  public inRight: boolean = false;
  public counter: number = 0;

  constructor() {
    this.showedProductsIndex = 2;
  }

  public _onNext(): void {
    this.counter++
    if (this.showedProductsIndex < (this.products.length - 1)) {
      this.showedProductsIndex++;
    }

  }

  public _onPrevious(): void {
    this.counter--;
    if (this.showedProductsIndex > 0) {
      this.showedProductsIndex--;
    }
  }
}
