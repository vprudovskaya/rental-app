import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {DataService} from "../services/data.service";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.less']
})
export class MapComponent implements OnInit {
  public addresses!: any[];
  public selectedAddress!: any;
  public isSelectingAvailable: boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private productService: DataService,
              public dialogRef: MatDialogRef<MapComponent>) {
    if (this.data) {
      this.isSelectingAvailable = true;
    } else {
      this.isSelectingAvailable = false;
    }
  }

  ngOnInit(): void {
    if (!this.data) {
      this.getProductAddresses();
    } else {
      this.addresses = this.data.addresses;
      this.setPlacemarkProperties();
      this.setPlacemarkOptions();
    }
  }

  public _selectAddress(address: any): void {
    this.selectedAddress = address;
  }

  public _close(): void {
    this.dialogRef.close(this.selectedAddress);
  }

  private setPlacemarkProperties(): void {
    for (let i = 0; i < this.addresses.length; i++) {
      this.addresses[i].property = {
        balloonContent: this.addresses[i].address,
      }
    }
  }

  private setPlacemarkOptions(): void {
    for (let i = 0; i < this.addresses.length; i++) {
      this.addresses[i].option = {
        preset: 'islands#darkBlueDotIcon'
      }
    }
  }

  private getProductAddresses(): void {
    this.productService.getAllPoints().subscribe((results) => {
      this.addresses = Object.values(results);
      this.setPlacemarkProperties();
      this.setPlacemarkOptions();
    });
  }
}
