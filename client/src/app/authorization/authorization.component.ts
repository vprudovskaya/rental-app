import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../services/auth.service";
import {ActivatedRoute} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {LoginErrorDialogComponent} from "../modal-windows/login-error-dialog/login-error-dialog.component";
import {RoutingService} from "../services/routing.service";

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.less']
})
export class AuthorizationComponent implements OnInit {
  public loginForm!: FormGroup;

  constructor(private auth: AuthService,
              private activatedRoute: ActivatedRoute,
              private dialog: MatDialog,
              private router: RoutingService) {
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(4)])
    });
  }

  public _goToStartPage(): void {
    this.router.goToStartPage();
  }

  public _onSubmit(): void {
    this.loginForm.disable();
    this.auth.login(this.loginForm.value).subscribe(
      () => {
        if (localStorage.getItem('auth-token')) {
          this.router.goToCatalog();
        }
      },
      error => {
        console.warn(error.status);
        this.showDialog();
        this.loginForm.enable();
        this.loginForm.reset();
      }
    )
  }

  private showDialog(): void {
    this.dialog.open(LoginErrorDialogComponent, {
      height: '442px',
      width: '700px',
    });
  }
}
